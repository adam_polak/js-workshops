const triangleAreaStrategy = (a, h) => measurements => a * h / 2;
const circleAreaStrategy = r => measurements => Math.PI * Math.pow(r, 2);

module.exports = {
  triangleAreaStrategy,
  circleAreaStrategy
};