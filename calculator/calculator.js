class Calculator {
  constructor(equation) {
    const splitEquation = equation.split('');

    this.numbers = [];
    this.operations = [];
    this.handlers = [
      SubHandler
    ];

    splitEquation.forEach(value => {
      if (!NaN(value)) {
        this.numbers.push(+value);
      } else {
        this.operations.push(value);
      }
    });
  }

  calc() {
    let total = this.numbers[0];

    this.operations.forEach((operation, index) => {
      const handler = this.handlers.find(handler => handler.isSatisfiedBy(operation));

      if (handler === undefined) {
        throw new Error('Nie ma takiego dzialania');
      }

      total = handler.compute(total, this.numbers[index+1]);
    });

    return total;
  }
}

module.exports = Calculator;

{
  availableDays: 21-26,
  nz: 0,
  ok: 0,
  workYears: 3
}


System(user).weUrlop(jakis, 15);
