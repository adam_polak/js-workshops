const Shape = require('./shape');

class Square extends Shape {
  constructor(a) {
    super([a, a, a ,a]);
  }
}

module.exports = Square;