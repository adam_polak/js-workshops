const Shape = require('./shape');

class Rectangle extends Shape{
  constructor(a, b) {
    super([a, b, a, b]);
  }
}

module.exports = Rectangle;