const lastToFirstStrategy = {
  board(passengers) {
    const sortedPassengers = passengers.slice().sort((a, b) => b.ticket.row - a.ticket.row);

    sortedPassengers.forEach((passenger) => console.log(passenger));
  }
};

const outerToInnerStrategy = pairs => ({
  board(passengers) {
    const filterByPair = (pair, passengers) => passengers
      .filter(passenger => passenger.ticket.sector === pair[0] || passenger.ticket.sector === pair[1]);

    pairs.forEach(pair => filterByPair(pair, passengers).forEach((passenger) => console.log(passenger)));
  }
});

module.exports = {
  lastToFirstStrategy,
  outerToInnerStrategy
};