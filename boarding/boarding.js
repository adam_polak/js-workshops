const createBoarding = (boardingStrategy) => ({
  start(paxes) {
    boardingStrategy
      .board(paxes);
  }
});

module.exports = {
  createBoarding
};