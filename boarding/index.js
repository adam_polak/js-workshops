const { createBoarding } = require('./boarding');
const { lastToFirstStrategy, outerToInnerStrategy } = require('./boarding.strategies');


const paxes = [
  {
    name: 'Adam',
    ticket: {
      row: 1,
      sector: 'A'
    }
  },
  {
    name: 'Adam',
    ticket: {
      row: 20,
      sector: 'D'
    }
  },
  {
    name: 'Adam',
    ticket: {
      row: 2,
      sector: 'F'
    }
  },
  {
    name: 'Adam',
    ticket: {
      row: 15,
      sector: 'C'
    }
  },
  {
    name: 'Adam',
    ticket: {
      row: 3,
      sector: 'B'
    }
  },
  {
    name: 'Adam',
    ticket: {
      row: 2,
      sector: 'E'
    }
  }
];

const lastToFirstBoarding = createBoarding(lastToFirstStrategy);
lastToFirstBoarding.start(paxes);

const outerToInnerBoarding = createBoarding(outerToInnerStrategy([
  ['A', 'F'],
  ['B', 'E'],
  ['C', 'D']
]));
outerToInnerBoarding.start(paxes);