const { triangleAreaStrategy, circleAreaStrategy } = require('./area.strategies');
const axios = require('axios');

const delay = (time) => new Promise((resolve, reject) => {
  setTimeout(() => resolve(), time)
});

const delayOld = (time, callback) => {
 setTimeout(() => callback(), time)
};

describe('Area strategies', () => {
  it('calculates triangle area', async (done) => {
    jest.setTimeout(10000);

    axios.get('https://reqres.in/api/users')
      .then(res => {
        console.log(res.data);
      })
  });
});

// (a, h) => measurements => a * h / 2;