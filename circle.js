const Shape = require('./shape');
const { circleAreaStrategy } = require('./area.strategies');

class Circle extends Shape {
  constructor(r) {
    super([r], circleAreaStrategy(r));
  }

  getPerimeter() {
    return 2 * Math.PI * this.r;
  }
}

module.exports = Circle;