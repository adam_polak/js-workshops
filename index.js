// class Cos {
//   constructor(bok1) {
//     this.bok = bok1;
//   }
//
//   getBok() {
//     return this.bok;
//   }
// }
//
// class Cos2 extends Cos{
//   constructor(bok2) {
//     super(50);
//
//     this.bok2 = bok2;
//   }
//
//   getBok2() {
//     return this.bok2;
//   }
// }
//
// class Cos3 extends Cos{
//   constructor(bok2) {
//     super(100);
//
//     this.bok2 = bok2;
//   }
//
//   getBok() {
//     return this.bok2 * 2;
//   }
//
//   getBok2() {
//     return this.bok2;
//   }
// }
//
// class Cos4 extends Cos{
//   constructor(bok2, strategiaLiczeniaPola) {
//     super(100);
//
//     this.bok2 = bok2;
//     this.strategia = strategiaLiczeniaPola;
//   }
//
//   getPole() {
//     return this.strategia();
//   }
//
//   getBok() {
//     return this.bok2 * 2;
//   }
//
//   getBok2() {
//     console.log(this.bok2);
//     const self = this;
//     function cos2() {
//       return self.bok2 * 2;
//     }
//
//     const cos = (a, b) => this.bok2;
//
//     return cos(this.a);
//   }
// }
//
// function liczPoleKwadratu() {
//   return 2 * 2;
// }
//
// function liczPoleKola() {
//   return 3 * 2;
// }
//
// const cos = new Cos(1234);
// console.log(cos.getBok());
//
// const cos2 = new Cos2(1234);
// console.log(cos2.getBok());
// console.log(cos2.getBok2());
//
// const cos3 = new Cos3(1234);
// console.log(cos3.getBok());
//
// const cos4 = new Cos4(123, liczPoleKola);
// console.log(cos4.getPole());

const Square = require('./square');
const Triangle = require('./trinagle');
const Shape = require('./shape');
const Circle = require('./circle');
const Rectangle = require('./rectangle');

// const square = new Square(5);
// console.log(square.getArea());
//
// const rectangle = new Rectangle(5, 10);
// console.log(rectangle.getArea());

// const shape = new Shape([10,20,3]);
// console.log(shape.getPerimeter());
//
// const square = new Square(4);
// console.log(square.getPerimeter());


// const shape = new Shape([3, 4, 5, 4], triangleAreaPattern);
// console.log(shape.getPerimeter());
// console.log(shape.getArea());

const triangle = new Triangle(3, 4, 5, 4);
console.log(triangle.getArea());
console.log(triangle.getPerimeter());

const circle = new Circle(1);
console.log(circle.getArea());
console.log(circle.getPerimeter());