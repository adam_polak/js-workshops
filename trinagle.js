const Shape = require('./shape');
const { triangleAreaStrategy } = require('./area.strategies');

class Triangle extends Shape {
  constructor(a, b, c, h) {
    super([a, b, c], triangleAreaStrategy(a, h));
  }
}

module.exports = Triangle;