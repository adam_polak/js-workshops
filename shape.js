class Shape {
  constructor(measurements, areaStrategy) { // Dependency injection & Strategy pattern
    this.measurements = measurements;
    this.areaStrategy = areaStrategy;
  }

  getArea() {
    return this.areaStrategy(this.measurements);
  }

  getPerimeter() {
    return this.measurements.reduce((total, measurement) => total + measurement);
  }
}

module.exports = Shape;