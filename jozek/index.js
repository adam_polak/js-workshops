const user = {
  availableDays: 26,
  onDemand: 0,
  occasional: 0,
  // workYears: 3
}

const { availableDaysY, onDemandY, occasionalY } = require('./strategies2');

const availableDays = {
  execute(user, days) {
    if (!user.availableDays >= days) {
      throw new Error('not enought days');
    }

    user.availableDays -= days;
  }
};


class VacationDayCalculator {
  constructor(user) {
    this.user = user;
    this.handlers = [
      availableDays,
      onDemandY,
      occasionalY
    ];
  }

  takeVacation(vacationType ,day) {
    const handler = this.handlers.find(handler => handler.isSatisfiedBy(vacationType));

    if (!handler) {
      throw new Error('asdadasd');
    }

    handler.execute(this.user, day);
  };
}

const vacCalc = new VacationDayCalculator(user)

console.log(vacCalc.takeVacation(availableDaysY,10))

module.exports = VacationDayCalculator
